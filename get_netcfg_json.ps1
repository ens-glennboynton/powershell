#
# File: get_netcfg_json.ps1
#
# Retrieve Network Adapter Configuration Detail
#  and output detail in JSON format.
#

if( $args[0] )
{
	$hostname = $args[0]
}
else
{
	$hostname = "localhost"
}
 

$netAdapters = Get-wmiobject -cl "Win32_NetworkAdapter" -comp $hostname -filter `
		"AdapterType = 'Ethernet 802.3' AND NetConnectionStatus = 2" #> 0"

if( $netAdapters )
{


Write-Host '{ "network" : { '
Write-Host `t '"host" : '"""$hostname"","
Write-Host `t '"Adapters" : ['

	ForEach( $adapter in $netAdapters )
	{

		$adpConf = @($adapter.GetRelated('Win32_NetworkAdapterConfiguration'))[0]
    Write-Host `t`t '{ "id" : '"""$($adapter.DeviceID)"","
    Write-Host `t`t '  "name" : '"""$($adapter.ServiceName)"","
    Write-Host `t`t '  "manufacturer" : '"""$($adapter.Manufacturer)"","
    Write-Host `t`t '  "type" : '"""$($adapter.AdapterType)"","
    Write-Host `t`t '  "fqdn" : '"""$($adpConf.DNSHostName)"","

    if( $adpConf.IPEnabled )
    {
        Write-Host `t`t '  "mac" : '"""$($adpConf.MACAddress)"","
    } else {
      Write-Host `t`t '  "mac" : '"""$($adpConf.MACAddress)"""      
    }

		if( $adpConf.IPEnabled )
		{
			Write-Host `t`t '  "ip" : '"""$($adpConf.IPAddress)"","
			Write-Host `t`t '  "mask" : '"""$($adpConf.IPSubnet)"","
			Write-Host `t`t '  "gateway" : '"""$($adpConf.DefaultIPGateway)"","
			Write-Host `t`t '  "domain" : '"""$($adpConf.DNSDomain)"","
			Write-Host `t`t '  "dns" : '"""$($adpConf.DNSServerSearchOrder)"","
			Write-Host `t`t '  "wins" : '"""$($adpConf.WINSPrimaryServer)"","
		}

		if( $adpConf.DHCPEnabled -and $adpConf.DHCPLeaseObtained )
		{
			Write-Host `t`t '  "wins2" : '"""$($adpConf.WINSSecondaryServer)"","
    } else {
      Write-Host `t`t '  "wins2" : '"""$($adpConf.WINSSecondaryServer)"""
    }
		
		if( $adpConf.DHCPEnabled -and $adpConf.DHCPLeaseObtained )
		{
			Write-Host `t`t '  "dhcp" : '"""$($adpConf.ConvertToDateTime( $adpConf.DHCPLeaseObtained ))"","
			Write-Host `t`t '  "expires" : '"""$($adpConf.ConvertToDateTime( $adpConf.DHCPLeaseExpires ))"","
			Write-Host `t`t '  "server" : '"""$($adpConf.DHCPServer)"""
		}

    if ($adapter -eq $netAdapters[-1])
    {
      Write-Host `t`t' }'  
    } else {
      Write-Host `t`t' },'
    }
    
	}
  Write-Host `t`t']'
  Write-Host `t'}'
  Write-Host '}'
} else {
  Write-Host "{ ""Status"": ""No Network Adapters Found"" }"
}